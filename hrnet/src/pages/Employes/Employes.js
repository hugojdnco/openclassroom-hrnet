import './Employes.css';
import { Text, DataTable, Data, Toolbar, DataSearch, Pagination } from 'grommet';
import { useSelector } from 'react-redux'
import { useState, useEffect } from 'react';
import { selectListEmployees } from './../../app/features/employees';
import { NavLink } from "react-router-dom";


function Employes() {

    const employees_selector = useSelector(selectListEmployees);
    const [employees, setEmployees] = useState(employees_selector);
    useEffect(() => {
        setEmployees(employees_selector);
    }, [employees_selector])

    return (
        <div id="employee-div" className="container">
            <h1>Current Employees</h1>
            <Data
                data={employees && employees}
            >
                <Toolbar><DataSearch /></Toolbar>
                <DataTable
                    columns={[
                        {
                            property: 'firstname',
                            header: <Text>First Name</Text>,
                            primary: true,
                        },
                        {
                            property: 'lastname',
                            header: <Text>Last Name</Text>,
                            primary: true,
                        },
                        {
                            property: 'startdate',
                            header: <Text>Start Date</Text>,
                            primary: true,
                        },
                        {
                            property: 'department',
                            header: <Text>Sales</Text>,
                            primary: true,
                        },
                        {
                            property: 'dateofbirth',
                            header: <Text>Date Of Birth</Text>,
                            primary: true,
                        },
                        {
                            property: 'street',
                            header: <Text>Street</Text>,
                            primary: true,
                        },
                        {
                            property: 'city',
                            header: <Text>City</Text>,
                            primary: true,
                        },
                        {
                            property: 'state',
                            header: <Text>State</Text>,
                            primary: true,
                        },
                        {
                            property: 'zipcode',
                            header: <Text>Zipcode</Text>,
                            primary: true,
                        },
                    ]}
                    sortable={true}
                    paginate={{
                        step: 2
                    }}
                />
                <Pagination />
            </Data>
            <NavLink to="/">Home</NavLink>
        </div>
    )
}
export default Employes;