import './Home.css';
import React from 'react';
import { DateInput, Select } from 'grommet';
import { states } from './../../data/states.js';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addEmployee } from './../../app/features/employees';
import { NavLink } from "react-router-dom";

import { Modal } from 'holek_oc_modal_component';


function Home() {
    const [value] = React.useState();
    const dispatch = useDispatch()

    const [firstname, setFirstName] = useState();
    const [lastname, setLastName] = useState();
    const [dateOfBirth, setDateOfBirth] = useState();
    const [startDate, setDateStart] = useState();
    const [street, setStreet] = useState();
    const [city, setCity] = useState();
    const [state, setState] = useState();
    const [department, setDepartment] = useState();
    const [zipCode, setZipCode] = useState();

    const [modalVisible, setModalVisible] = useState(false);

    const showModal = () => {
        setModalVisible(true);
    };

    const hideModal = () => {
        setModalVisible(false);
    };


    function handleChangeFirstName(e) {
        setFirstName(e.target.value);
    }

    function handleChangeLastName(e) {
        setLastName(e.target.value);
    }

    function handleChangeStreet(e) {
        setStreet(e.target.value);
    }

    function handleChangeCity(e) {
        setCity(e.target.value);
    }

    function handleZipCode(e) {
        setZipCode(e.target.value);
    }

    function saveEmployee() {
        const employee = {
            'firstname': firstname,
            'lastname': lastname,
            'startdate': startDate,
            'department': department,
            'dateofbirth': dateOfBirth,
            'street': street,
            'city': city,
            'state': state,
            'zipcode': zipCode,
        };
        dispatch(addEmployee(employee))
        showModal();
    }

    return (
        <main>
            <div className="title">
                <h1>HRnet</h1>
            </div>
            <div className="container">
                <NavLink to="/list-employes">View employees</NavLink>
                <h2>Create Employee</h2>
                <form action="#" id="create-employee">
                    <label htmlFor="first-name">First Name</label>
                    <input type="text" id="first-name" onChange={handleChangeFirstName} />

                    <label htmlFor="last-name">Last Name</label>
                    <input type="text" id="last-name" onChange={handleChangeLastName} />

                    <label htmlFor="date-of-birth">Date of Birth</label>
                    <DateInput
                        format="dd/mm/yyyy"
                        id="date-of-birth"
                        value={value}
                        onChange={({ value }) => setDateOfBirth(value)}
                    />

                    <label htmlFor="start-date">Start Date</label>
                    <DateInput
                        format="dd/mm/yyyy"
                        id="start-date"
                        value={value}
                        onChange={({ value }) => setDateStart(value)}
                    />

                    <fieldset className="address">
                        <legend>Address</legend>

                        <label htmlFor="street">Street</label>
                        <input id="street" type="text" onChange={handleChangeStreet} />

                        <label htmlFor="city">City</label>
                        <input id="city" type="text" onChange={handleChangeCity} />

                        <label htmlFor="state">State</label>
                        <Select
                            options={states}
                            value={value}
                            id="state"
                            onChange={({ option }) => setState(option)}
                        />

                        <label htmlFor="zip-code">Zip Code</label>
                        <input id="zip-code" type="number" onChange={handleZipCode} />
                    </fieldset>

                    <label htmlFor="department">Department</label>
                    <Select
                        options={['Sales', 'Marketing', 'Engineering', 'Human Resources', 'Legal']}
                        value={value}
                        id="departement"
                        onChange={({ option }) => setDepartment(option)}
                    />
                </form>
                <Modal show={modalVisible} onClose={hideModal}>
                    <p>Employee created</p>
                </Modal>
                <button onClick={saveEmployee}>Save</button>
            </div>
        </main>
    )
}
export default Home;