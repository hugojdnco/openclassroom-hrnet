import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    employees: [],
};

export const ListEmployeesSlice = createSlice({
    name: 'listEmployees',
    initialState,

    reducers: {
        addEmployee: (state, action) => {
            state.employees.push(action.payload)
        },
    },
});

export const { addEmployee } = ListEmployeesSlice.actions;

export const selectListEmployees = (state) => state.employees.employees;

export default ListEmployeesSlice.reducer;